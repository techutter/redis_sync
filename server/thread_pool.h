#ifndef THREAD_POOL_H
#define THREAD_POOL_H 1

#include <pthread.h>
 
/* 要执行的任务链表 */
typedef struct tpool_work {
    void*               (*routine)(void*,void **,void*);       /* 任务函数 */
    void                *arg;                    /* 传入任务函数的参数 */
    struct tpool_work   *next;                    
}tpool_work_t;

/* 线程池 */
typedef struct tpool {
    int             shutdown;                   /* 线程池是否销毁 */
    int             max_thr_num;                /* 最大线程数 */
    pthread_t       *thr_id;                    /* 线程ID数组 */
    tpool_work_t    *queue_head;                /* 线程链表 */
    pthread_mutex_t queue_lock;                    
    pthread_cond_t  queue_cond_can_get;
    pthread_cond_t  queue_cond_can_put;
    void            *context;                   /* 线程池自定义上下文数据 */
    void*(*thd_ctx_init_func)(void *,void**);          /* 线程上下文数据的初始化函数 */
    void*(*thd_ctx_fini_func)(void *,void*);           /* 线程上下文数据的销毁函数 */
} tpool_t;

/*
 * @brief     创建线程池 
 * @param     max_thr_num 最大线程数
 * @return    0: 成功 其他: 失败  
 */
tpool_t * tpool_create(int max_thr_num, void * context, 
    void*(*thd_ctx_init_func)(void *,void**), void*(*thd_ctx_fini_func)(void *,void*));

/*
 * @brief     销毁线程池 
 */
void tpool_destroy(tpool_t * tpool);

/*
 * @brief     向线程池中添加任务
 * @param    routine 任务函数指针
 * @param     arg 任务函数参数
 * @return     0: 成功 其他:失败 
 */
int tpool_add_work(tpool_t * tpool, void*(*routine)(void*,void **,void*), void *arg);

#endif