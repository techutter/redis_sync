#ifndef SYNC_H
#define SYNC_H 1

#include <hiredis/hiredis.h>
#include "thread_pool.h"
#include "utarray.h"

#include <limits.h>  //PATH_MAX
#include <pthread.h>
#include <sys/time.h> /* for struct timeval */

#define MSG_PROJECT_ID   78
#define MSG_QUEUE_PERMS  0777


/* 线程池上下文 */
typedef struct pool_context {
    char ip_buf[20];
    int port;
    struct timeval timeout_tv;
} pool_context_t;


/* 消息队列配置 */
typedef struct msg_queue_item {
    char queue_path[PATH_MAX];
    char sync_nodes_list[PATH_MAX];
    UT_array *sync_nodes_array;
    int loop_thread_running;
    pthread_t loop_thread_id;
    int thread_num; 
    int timeout_secs;
    struct timeval timeout_tv;
    pool_context_t  *pool_cxt;
    tpool_t **pools;
    int pool_num;
    int msg_system_id;
} msg_queue_item_t;


/* 线程上下文 */
typedef struct thread_context {
    redisContext *conn;
} thread_context_t;


/**
** 解析出数组,逗号分隔的字符串
**/ 
UT_array * parse_array(char * value);


/**
** 初始化
**/
int init_sync();


/**
** 销毁
**/
int fini_sync();


/**
** 初始化队列循环
**/
int init_msg_queue_sync(msg_queue_item_t * msg_queue_ic);


/**
** 销毁队列循环
**/
int fini_msg_queue_sync(msg_queue_item_t * msg_queue_ic);


/**
** 处理redis命令
** 需要对所有的配置节点都执行这个命令
**/
int sync_redis_command(msg_queue_item_t * msg_queue_ic, const char * param_command);


#endif
