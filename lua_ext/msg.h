#ifndef MSG_H
#define MSG_H 1

#define MSG_MAX_LEN 60

struct sync_msg_buf {
    long mtype;                 /* message type, must be > 0 */
    char mtext[MSG_MAX_LEN];    /* message data */
};

#endif /*MSG_H*/

